#include <iostream>
#include <string>
using namespace std;



int main(){
	int num_alumnos, cont_no_rindio_prueba, cont_nota_1, cont_nota_1_2, cont_nota_2_3, cont_nota_3_4, cont_nota_4_6, cont_nota_6_7 = 0;
	float nota = 0;
	cout<<"Ingrese el numero de alumnos:";
	cin >> num_alumnos;

	for (int i=0; i< num_alumnos; i++){
		cout<<"Nota del alumno ["<<i<<"]: ";
		cin >> nota;

		if (nota == 0){
			cont_no_rindio_prueba ++;
		} else if (nota == 1) {
			cont_nota_1 ++;
		} else if (nota > 1 && nota < 2){
			cont_nota_1_2 ++;
		} else if (nota >= 2 && nota < 3){
			cont_nota_2_3 ++;
		} else if (nota >= 3 && nota < 4){
			cont_nota_3_4 ++;
		} else if (nota >=4 && nota < 6){
			cont_nota_4_6 ++;
		} else if (nota >=6 && nota <= 7){
			cont_nota_6_7 ++;
		}

	}

	cout<<"Alumnos que rindieron la prueba: "<<num_alumnos-cont_no_rindio_prueba<<endl;

	cout <<"Alumnos que no rindieron la prueba: "<<cont_no_rindio_prueba<<endl;

	float p_rindieron = float(num_alumnos- cont_no_rindio_prueba)/num_alumnos;
	cout <<"% de alumnos que rindieron la prueba:"<<p_rindieron*100<<endl;

	cout<<"Cantidad de alumnos que obtuvieron un 1: "<<cont_nota_1<<endl;
	
	cout<<"Cantidad de alumnos que obtuvieron nota > 1 y < 2: "<<cont_nota_1_2<<endl;

	cout<<"Cantidad de alumnos que obtuvieron nota >= 2 y < 3: "<<cont_nota_2_3<<endl;

	cout<<"Cantidad de alumnos que obtuvieron nota >= 3 y < 4: "<<cont_nota_3_4<<endl;

	cout<<"Cantidad de alumnos que obtuvieron nota >=4  y < 6: "<<cont_nota_4_6<<endl;

	cout<<"Cantidad de alumnos que obtuvieron nota >= 6 y <= 7: "<<cont_nota_6_7<<endl;

	return 0;
}

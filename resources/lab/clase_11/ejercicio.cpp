#include <iostream>
#include <vector>
using namespace std;

int main() {

  const int largo = 5;
  int suma =0;
  int primos=0;
  int multi=1;
  int array[largo]; // int array[5] --ok

  for (int i=0; i< largo; i++){ //for (int i=0; i<5; i++) --ok
    cout<<"Ingrese numero "<<i<<":";
    cin>>array[i];
  }

  //Imprimir arreglo
  for (int i=0; i< largo; i++){ //for (int i=0; i<5; i++) --ok
    cout<<array[i]<<" ";
    suma = suma + array[i];
    multi = multi * array[i];
  }

  cout<<"La suma es: "<<suma<<endl;
  cout<<"La multiplicacion es: "<<multi<<endl;
  
  //Contador de numeros primos
  for (int i=0; i< largo; i++){ 
        if (array[i] == 1){
          primos++;
        } else{
            for (int j=2; j<= array[i]; j++){ 
              if ( (array[i] % j) == 0 && array[i] != j){ // El NUMERO  NO ES PRIMO
                  break;
              }else if( (array[i] % j) == 0 && array[i] == j){ // El NUMERO  ES PRIMO
                  primos++;
              }
            }
        }         
  }

  cout<<"Se encontraron: "<<primos<<" numeros primos"<<endl;
  return 0;
}



#include <iostream>
#include <vector>
using namespace std;

int main() {


  vector<int> num {1, 2, 3, 4, 5};

  cout << "Initial Vector: ";

  for (int i=0; i<num.size(); i++) {
    cout << num[i] << "  ";
  }
  
  // add the integers 6 and 7 to the vector
  num.push_back(6);
  num.push_back(7);

  cout << "\nUpdated Vector: ";

  for (int i=0; i<num.size(); i++){
    cout << num[i] << "  ";
  }

  return 0;
}
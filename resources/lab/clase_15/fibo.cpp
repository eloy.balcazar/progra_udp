#include <iostream>
#include <vector>
#include "string"
using namespace std;

int fiboNachi(int anterior, 
              int actual, 
              int numSerieFinal, 
              int numSerieActual){
  if (numSerieActual == numSerieFinal){
    return anterior+actual;
  }

  int newActual = anterior+actual;
  return fiboNachi(actual, newActual, numSerieFinal,  numSerieActual+1);
}

int main() {
  int num =0;
  cout<<"Ingrese el numero de fibo"<<endl;
  cin>>num;

  if (num == 1 || num == 2){
    cout<<"r:"<<1<<endl;
  } else{
    num -= 2;
    int resultado = fiboNachi(1, 1, num, 1);
    cout<<resultado<<endl;
    return 0;
  }


}



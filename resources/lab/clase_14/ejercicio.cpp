#include <iostream>
#include <vector>
#include "string"
using namespace std;

bool asientoLibre(int asiento){
  cout<<"es libre?: "<<asiento<<endl;
  if (asiento == 0){
    return true;
  }
  return false;
}

int getFila(char letra){
  return int(letra)-65;
}

void imprimir(int array[][26]){
   for (int i=0; i<6; i++){
    cout<<endl;
    for (int j=0; j<26; j++){
      if (j>0){
        cout<<"|";
      }
      cout<<array[i][j];
    }
  }
  cout<<endl;
}

int main() {

  int asientos[6][26];
  //INICIAN ASIENTOS
  for (int i=0; i<6; i++){
    for (int j=0; j<26; j++){
      asientos[i][j]=0;
    }
  }
  
  imprimir(asientos);

  bool exit = false;
  while (!exit){
    char fila;
    int columna;
    string cont="true";
    cout<<"Ingrese Fila [A -> F]:";
    cin>>fila;
    cout<<"Ingrese la Columna:";
    cin>>columna;

    //VALIDA ASIENTO LIBRE
    bool isFree = asientoLibre(asientos[getFila(fila)][columna]);
    //RESERVA
    if (isFree){
      asientos[getFila(fila)][columna]=1;
    }else{
      cout<<"El asiento esta ocupado"<<endl;
    }

    imprimir(asientos);

    cout<<"Continuar?: [yes/no]:";
    cin>>cont;
    if (cont == "no"){
      exit=true;
    }
  }

  return 0;
}



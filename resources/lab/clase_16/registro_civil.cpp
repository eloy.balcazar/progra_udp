#include <iostream>
#include <vector>
#include "string"
using namespace std;

struct persona{
  int rut;
  string nombre;
  string apellido;
  int edad;
};


void printPersonas(vector<persona> personas);
bool exitCheck();

int main() {
  vector <persona> personas;
  bool exit = false;
  while (!exit){
      persona p;
      cout<<"Ingrese rut sin el gion:";
      cin >> p.rut;
      cout<<"Ingrese Nombre:";
      cin >> p.nombre;
      cout<<"Ingrese Apellido:";
      cin >> p.apellido;
      cout<<"Ingrese Edad:";
      cin >> p.edad;
      personas.push_back(p); //agregando a la nueva persona 
      exit = exitCheck(); 
  }

  printPersonas(personas);
  return 0;
}


void printPersonas(vector<persona> personas){
  for (int i=0; i< personas.size(); i++){
    cout<<"rut:"<<personas[i].rut<<", nombre:"<<personas[i].nombre<<", apellido:"<<personas[i].apellido<<", edad:"<<personas[i].edad<<endl;
  }
}


bool exitCheck(){
      string opt;
      cout<<"Desea agregar a otra persona? [y/n]:";
      cin >> opt;
      if (opt == "y" || opt == "Y"){
        return false;
      }
      return true;
}

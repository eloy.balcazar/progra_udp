#include <iostream>
using namespace std;

int main(){
	//Variables
	int ladoA, ladoB, ladoC = 0;
	cout<<"Ingrese el lado A"<<endl;
	cin >> ladoA;
        cout<<"Ingrese el lado B"<<endl;
        cin >> ladoB;
        cout<<"Ingrese el lado C"<<endl;
        cin >> ladoC;
	//Logica
	if (ladoA == ladoB && ladoB == ladoC){
		cout<<"Es un triangulo Equilatero - todos sus lados son iguales"<<endl;
	}else if (ladoA == ladoB || ladoB == ladoC || ladoA == ladoC){
		cout<<"Es un triangulo Isosceles - solo tiene dos lados iguales"<<endl;
	}else{
		cout<<"Es un triangulo Escaleno - todos sus lados son diferentes"<<endl;
	}
	return 0;
}



#include <iostream>
#include <vector>
using namespace std;

void cuentaRegresiva(int x){
  cout<<" Instante "<<x<<endl;
  if (x > 0){
    cuentaRegresiva(x-1);
  }
}


int main() {
  int entero = 0;
  cin>> entero;
  cuentaRegresiva(entero);
  return 0;
}



#include <iostream>
#include <string>
using namespace std;



int main(){
	int numAlumnos, numAlumnosAnterior, numCursos =0;
	string nombreCurso, nombreCursoAnterior = "N.A";
	
	cout<<"INGRESE NUMERO DE CURSOS A EVALUAR:";
	cin >>  numCursos;
	for (int i = 0; i < numCursos ; i++){
		cout<<"INGRESE EL NOMBRE DEL CURSO: ";
		cin >> nombreCurso;
		cout<<"INGRESE EL NUMERO DE ALUMNOS:";
		cin >> numAlumnos;

		if (numAlumnos > numAlumnosAnterior){
			numAlumnosAnterior = numAlumnos;
			nombreCursoAnterior = nombreCurso;
		}
	}

	cout <<"EL CURSO CON MAS ALUMNOS ES: "<<nombreCursoAnterior<<endl;

	return 0;
}

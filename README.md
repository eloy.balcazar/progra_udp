# Curso de Programación

## Tools
* [Docencia EIT](https://docencia-eit.udp.cl/app)
* [Editor de texto - VS Codium](https://vscodium.com/)
* [GCC / G++](https://www.bloodshed.net/)
* [GIT UDP](https://giteit.udp.cl/)

# Programa del Curso
* [Programa del Curso](/resources/Programa.pdf)
* [Cronograma del Curso](/resources/planificacion/planificaci%C3%B3n%20progra.xlsx)

# Clases
1. Introducción al curso - [ODP](/resources/clases/p_1_introduccion.odp) | [PDF](/resources/clases/p_1_introduccion.pdf)
2. Varaibles - [ODP](/resources/clases/p_2_variables.odp) | [PDF](/resources/clases/p_2_variables.pdf)
3. Condiciones - [ODP](/resources/clases/p_3_condiciones.odp) | [PDF](/resources/clases/p_3_condiciones.pdf)
4. Ejercicios - [ODP](/resources/clases/p_4_ejercicio.odp) | [PDF](/resources/clases/p_4_ejercicio.pdf)
5. Laboratorio - [ODP](/resources/clases/p_5_laboratorio.odp) | [PDF](/resources/clases/p_5_laboratorio.pdf)
6. Switch && Ejercicios - [ODP](/resources/clases/p_6_review_laboratorio.odp) | [PDF](/resources/clases/p_6_review_laboratorio.pdf)
7. Ciclos part I - [ODP](/resources/clases/p_7_ciclos_part_I.odp) | [PDF](/resources/clases/p_7_ciclos_part_I.pdf)
8. Ciclos part II -[ODP](/resources/clases/p_8_ciclos_part_II.odp) | [PDF](/resources/clases/p_8_ciclos_part_II.pdf)
9. Ejercicios de ciclos -[ODP](/resources/clases/p_9_ejercicios.odp) | [PDF](/resources/clases/p_9_ejercicios.pdf)


